**How to use this dockerfile**

After cloning the repo in your system you need to do the following steps.

**Run this command from the directory where the repo has been clone**: 
docker build -t btc_demo .

**After the image was successfully built, we can run the image with this command**
docker run -d -it --restart unless-stopped \
--name demo-full-node \
-m 2000m \
--cpus=2 \
-p 18443:18443 \
-p 18444:18444 \
-v ${PWD}/data:/home/bitcoin/.bitcoin \
final_demo \
-printtoconsole \
-regtest=1 \
-rpcallowip=172.17.0.0/16 \
-rpcbind=0.0.0.0 \
-rpcuser='demouser' \
-rpcpassword='demopass'

---

**Whether the node is working can be checked by running this curl**

curl -v --request POST \
   --url http://demouser:demopass@localhost:18443/ \
   --header 'content-type: application/json' \
   --data '{"jsonrpc": "2.0","id": "sample-id","method": "getblockchaininfo","params": []}'

---

## Featues

* The container will autorestart on failure unless explicitly stopped. (--restart unless-stopped)
* Has resource consumption limit of 2gb memory and 2cpus at max.
* Performs a health check. (docker ps command will return the state of the container when it is runnning.)